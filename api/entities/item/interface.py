from mypy_extensions import TypedDict


class ItemInterface(TypedDict, total=False):
    name: str
