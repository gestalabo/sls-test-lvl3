from sqlalchemy import Integer, Column, String
from api.utils.DB import Base  # noqa
from .interface import ItemInterface
from typing import Any

class Item(Base):  # type: ignore
    """Items"""

    __tablename__ = "item"

    item_id = Column(Integer(), primary_key=True, autoincrement=True)
    name = Column(String(255))

    def update(self, changes: ItemInterface):
        for key, val in changes.items():
            setattr(self, key, val)
        return self