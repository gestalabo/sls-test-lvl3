# from app import db
from typing import List
from .model import Item
from .interface import ItemInterface

class ItemService:
    @staticmethod
    def get_all() -> List[Item]:
        return Item.query.all()

    @staticmethod
    def get_by_id(item_id: int) -> Item:
        return Item.query.get(item_id)

    @staticmethod
    def update(Item: Item, updated_item: ItemInterface, session) -> Item:
        Item.update(updated_item)
        session.commit()
        return Item

    @staticmethod
    def delete_by_id(item_id: int, session) -> List[int]:
        item = Item.query.filter(Item.item_id == item_id).first()
        if not Item:
            return []
        session.delete(Item)
        session.commit()
        return [item_id]

    @staticmethod
    def create(new_attrs: ItemInterface, session) -> Item:
        new_item = Item(name=new_attrs["name"])

        session.add(new_item)
        session.commit()

        return new_item
