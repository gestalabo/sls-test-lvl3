from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from .model import Item

class ItemSchema(SQLAlchemySchema):
    """Item schema"""
    class Meta:
        model = Item

    id = auto_field(column_name="item_id")
    name = auto_field()