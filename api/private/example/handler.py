import logging
from api.utils.Response import Response
from api.utils.DB import get_session, AlchemyEncoder
from api.entities.item.service import ItemService

logger = logging.getLogger()

def private(event, context):
    return Response._200(body=dict(message='Private Endpoint!'))