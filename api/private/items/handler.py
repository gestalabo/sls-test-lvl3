import logging
import json
from api.entities.item.service import ItemService
from api.entities.item.schema import ItemSchema
from api.utils.Response import Response
from api.utils.DB import get_session, AlchemyEncoder
from api.utils.printers import symbols_lines

logger = logging.getLogger()

# event['pathParameters'] . Parametros de ruta
# event['body'] Body del Request

def create(event, context):
    try:
        # print("/api/item: %s", event)
        session = get_session()
        inputBody = json.loads(event['body'])
        item = ItemService.create(inputBody, session)
        item_schema = ItemSchema()
        
        result = item_schema.dump(item)
        
        return Response._200(body=result, custom_encoder=AlchemyEncoder)

    except Exception as e:
        logger.exception("Exception during the execution: %s", e)
        return Response._500()

    