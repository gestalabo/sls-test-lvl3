import os
import json
import time
import urllib.request
import base64
import re
import sqlite3
from jose import jwk, jwt
from jose.utils import base64url_decode

from api.utils.Response import Response
from api.utils.printers import format_error, symbols_lines
# from cryptography.hazmat.backends import default_backend
# from cryptography.x509 import load_pem_x509_certificate

def auth(event, context):
    try:
        whole_token = event.get('authorizationToken')

        if not whole_token:
            # C-401-U1
            format_error("NO TOKEN", "C-401-U1")
            return Response._401()

        token = whole_token.split(' ')[1]
        kid = jwt.get_unverified_header(token).get("kid")

        # C-401-U4
        # use the unverified claims
        claims = jwt.get_unverified_claims(token)

        if not (claims['accessKey'] == 'QWERTYUIOP' and claims['role'] == 'bot'):
            return Response._401()
        
        connection = sqlite3.connect('test.db')
        cursor = connection.cursor()

        sql_file = open('1.sql')
        sql_as_string = sql_file.read()
        cursor.executescript(sql_as_string)

        # now we can use the claims
        policy = generate_policy(claims['sub'], 'Allow', event['methodArn'])

        return policy
    except Exception as e:
        # C-401-U7
        format_error("Fatal Error in authorizer.py: {}".format(e), "C-401-U7")
        return Response._500()
    
def generate_policy(principal_id, effect, resource):
    return {
        'principalId': principal_id,
        'policyDocument': {
            'Version': '2012-10-17',
            'Statement': [
                {
                    "Action": "execute-api:Invoke",
                    "Effect": effect,
                    "Resource": resource
                }
            ]
        }
    }