import json
import decimal
import datetime

class Response():

    FORMAT = {
        "headers":  {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True
        }
    }

    @staticmethod
    def _200(body: dict, custom_encoder=None) -> dict:

        format_body = json.dumps(None)

        custom_cls = None
        if not custom_encoder:
            custom_cls = _CustomJSONDecoder 
        else:
            custom_cls = custom_encoder

        if isinstance(body, dict):
            format_body = json.dumps(body, cls=custom_cls)

        Response.FORMAT["statusCode"] = 200
        Response.FORMAT["body"] = format_body

        return Response.FORMAT

    @staticmethod
    def _401() -> dict:

        Response.FORMAT["statusCode"] = 401
        Response.FORMAT["message"] = "Unauthorized!"

        return Response.FORMAT
    
    @staticmethod
    def _500() -> dict:

        Response.FORMAT["statusCode"] = 500
        Response.FORMAT["message"] = "Server Error!"

        return Response.FORMAT

class _CustomJSONDecoder(json.JSONEncoder):
    def default(self, o):
        # if isinstance(o, ObjectId):
        #     return str(o)
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()
        return super(_CustomJSONDecoder, self).default(o)