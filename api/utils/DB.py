import json 
import logging
import os
import sqlalchemy
from sqlalchemy.orm import sessionmaker 
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.orm import declarative_base

# host = os.getenv('DB')
engine = sqlalchemy.create_engine(f"sqlite:///test.db")
Session = sessionmaker(bind=engine)
Base = declarative_base()

def get_session():
    """Return a new session from the db engine"""
    return Session()

def get_engine():
    return engine

class AlchemyEncoder(json.JSONEncoder):
    """ Based on: https://stackoverflow.com/questions/5022066/how-to-serialize-sqlalchemy-result-to-json/41204271 """
    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) 
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            return fields
        return json.JSONEncoder.default(self, obj)
